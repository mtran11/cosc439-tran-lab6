#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

bool isWaiting = true;		//wait check status
int numberOfSlices = 0;	//number of slices have been eaten	
int pizzasID = 0; 		//pizza id is being eaten
sem_t mutexLockDelivering; 
sem_t mutexWait;

void *studyGroup(void *ptr);
void *pizzaProducing(void *ptr);

int main(int argc, char *argv[]) {
	isWaiting = true;
	sem_init(&mutexLockDelivering, 0, 0);
	sem_init(&mutexWait, 0, 1);
	unsigned int numberOfStudents = atoi(argv[1]);//convert string to int	
	
	if (numberOfStudents > 5 || numberOfStudents < 2) {
		printf("Number of students in study group and should be between 2-5 \n");
		return 0;
   	} else {
		pthread_t studentArray[numberOfStudents];
		int count;
		for (count = 0; count < numberOfStudents; count++) {
	   		pthread_create(&studentArray[count], NULL, studyGroup, (void*) count + 1);
		}
		pthread_t studentIsDelivering;
		pthread_create(&studentIsDelivering, NULL, pizzaProducing, (void*) studentIsDelivering);
		pthread_join(studentIsDelivering, NULL);
		//allow other student threads join
		for(int j = 0; j < numberOfStudents; j++) {
	   		pthread_join(studentArray[j], NULL);
		}
		pthread_exit(NULL);
   	}
   	sem_destroy(&mutexLockDelivering);
   	sem_destroy(&mutexWait);
   	return 0;
}

void *studyGroup(void *ptr) {
	long studentID = (long) ptr;
	sem_wait(&mutexWait);
  
	while(pizzasID < 13) { 
		if(numberOfSlices > 0) {
 			printf("Student %ld is eating a slice of pizza %d \n", studentID, pizzasID + 1);
	   		numberOfSlices--;
	   		if(numberOfSlices == 0) {
	      			pizzasID++;
	   		}
		} else if(numberOfSlices == 0) {
	   		if(isWaiting == true) {
				isWaiting = false;
				printf("Student %ld is ordering the pizza %d \n", studentID, pizzasID + 1);
				printf("Take a nap until another pizza arrives \n");
	   		} else if(isWaiting == false) {
				// Going to sleep until the sleep function returns
	        		sleep(1);
	        		// Guh! I’m awake!
	   		}	
	   		sem_post(&mutexLockDelivering);
		} else {
	   		break;
		}
		sem_post(&mutexWait);		
		sleep(1);		
   	}
   	return NULL;
}

void *pizzaProducing(void *ptr) {
	while(pizzasID < 13) {
		sem_wait(&mutexLockDelivering);
		numberOfSlices = 8;
		printf("Pizza %d is delivered \n", pizzasID + 1);
		isWaiting = true;
	}
	return NULL;
}
	
